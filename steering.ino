void goForward(float sp, float rotation) //speed = float 0-1, angle, float +1 -- -1, turn left <0 turn right >0, go straight at 0
{
  int damper = 2;

  if (state == 1)
  {
    //deviate path, never go straight line, deviate rotation by up to 10% every turn, maybe tweak and reduce, go slower, speed multiplier, random between 80% and 100% set speed
    sp=random(.8,1)*sp;
    rotation=rotation+random(-.1,.1);
  }
  else //no valid state, seize operations
  {
    changeAction(0, 0);

  }
  float left = sp - (rotation / damper);
  float right = sp + (rotation / damper);
  changeAction(left, right);

}

void fastMoveLeft() //as the name says
{
  changeAction(-1, 1); //Turn the robot left

}

void fastMoveRight()
{
  changeAction(1, -1); //Turn the robot right

}


void makePlayfulMovements() //fast left right shaking combination of the happy dolphin
{

}
